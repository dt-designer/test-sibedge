(function (global) {
    System.config({
        paths: {
            'npm:': '/node_modules/'
        },

        map: {
            main: 'src/main.js',
            app: 'src/app',

            '@angular/core': 'npm:@angular/core/bundles/core.umd.js',
            '@angular/common': 'npm:@angular/common/bundles/common.umd.js',
            '@angular/compiler': 'npm:@angular/compiler/bundles/compiler.umd.js',
            '@angular/platform-browser': 'npm:@angular/platform-browser/bundles/platform-browser.umd.js',
            '@angular/platform-browser-dynamic': 'npm:@angular/platform-browser-dynamic/bundles/platform-browser-dynamic.umd.js',
            '@angular/common/http': 'npm:@angular/common/bundles/common-http.umd.js',
            '@angular/forms': 'npm:@angular/forms/bundles/forms.umd.js',

            'rxjs': 'npm:rxjs',
            'rxjs-compat': 'npm:rxjs-compat',
            'rxjs/operators': 'npm:rxjs/operators',
        },

        packages: {
            'src/app': {},
            'rxjs': {'main': 'index.js','defaultExtension': 'js'},
            'rxjs/operators': {'main': 'index.js','defaultExtension': 'js'},
            'rxjs/internal-compatibility': {'main': 'index.js','defaultExtension': 'js'},
            'rxjs/testing': {'main': 'index.js','defaultExtension': 'js'},
            'rxjs/ajax': {'main': 'index.js','defaultExtension': 'js'},
            'rxjs/webSocket': {'main': 'index.js','defaultExtension': 'js'},
            'rxjs-compat': {'main': 'index.js','defaultExtension': 'js'},
            'core-js': {},
            'zone.js': {},
            "app": {
                main: "main",
                defaultExtension: "js",

                meta: {
                    "*.component.js": {
                        loader: "system.component-loader.js"
                    }
                }
            },
        }
    });
})(this);
