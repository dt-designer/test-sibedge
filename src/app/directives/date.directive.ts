import {Directive, Input, ElementRef, AfterViewInit} from '@angular/core';

@Directive({
    selector: '[date]'
})
export class DateDirective implements AfterViewInit {

    @Input() data: any;
    @Input() params: any;

    constructor(
        private _el: ElementRef
    ) {}

    ngAfterViewInit() {
        const date = this.data.split(' ');
        if (this.params.date) {
            this._el.nativeElement.insertAdjacentHTML('beforeend', '<span class="inner-date">'+date[0]+'</span>');
        }
        if (this.params.time) {
            this._el.nativeElement.insertAdjacentHTML('beforeend', '<span class="inner-time">'+date[1]+'</span>');
        }
    }
}
