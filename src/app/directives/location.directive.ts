import {Directive, Input, ElementRef, AfterViewInit} from '@angular/core';

// export interface IConfigItem {
//     path?: string;
//     block?: Array<string>|string;
// }

@Directive({
    selector: '[location]'
})
export class LocationDirective implements AfterViewInit {

    @Input() data: any;

    constructor(
        private _el:ElementRef
    ) {}

    ngAfterViewInit() {
        const html = '<span class="table-location">' +
                '<span class="city">'+this.data.city+'</span>' +
                '<span class="view-full"></span>' +
                '<span class="full-info">' +
                    '<span class="item">Регион: '+this.data.state+' ('+this.data.postcode+')</span>' +
                    '<span class="item">Город: '+this.data.city+'</span>' +
                    '<span class="item">Улица: '+this.data.street+'</span>' +
                '</span>' +
            '</span>';
        this._el.nativeElement.insertAdjacentHTML('beforeend', html);
    }
}
