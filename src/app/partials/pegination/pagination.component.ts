import {Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges} from '@angular/core';

@Component({
    selector: 'app-pagination',
	templateUrl: './pagination.component.html',
	styleUrls: ['./pagination.component.css']
})
export class PaginationComponent implements OnInit, OnChanges {
    pagination: any = [];

    @Input() page: any;
    @Input() pageLimits: any;
    @Input() currentLimit: any;
    @Input() currentTime: any;
    @Output() action = new EventEmitter<any>();

    constructor( ) { }

    ngOnInit() {
        this.buildPagination();
    }

    ngOnChanges(changes: SimpleChanges) {
        this.buildPagination();
    }

    buildPagination() {
        this.pagination = [];
        this.pagination.push({
            title: 'Первая',
            offset: 0,
            active: false,
            disabled: (this.page.offset >= 1) ? false : true
        });
        this.pagination.push({
            title: 'Пред.',
            offset: (this.page.offset >= 1) ? (this.page.offset - this.page.limit) : 0,
            active: false, disabled: (this.page.offset >= 1) ? false : true
        });
        let count = (this.page.total / this.page.limit);
        const tempCount = (count - parseInt(''+count));
        if (tempCount > 0) {
            count = parseInt('' + count) + 1;
        }
        let tempActive = 1;
        for (let i = 0; count > i; i++) {
            tempActive = (this.page.offset + this.page.limit) / this.page.limit;
            this.pagination.push({
                title: (i + 1),
                offset: (i * this.page.limit),
                active: ((i + 1) === tempActive) ? true : false,
                disabled: false
            });
        }
        if (count > 7) {
            if (tempActive < 4) {
                this.pagination.splice(6, count - 5, {
                    title: '...',
                    offset: 'showAfter',
                    active: false,
                    disabled: false
                });
            } else if (tempActive >= 4 && tempActive < count - 2) {
                this.pagination.splice(2, tempActive - 3, {
                    title: '...',
                    offset: 'showBefore',
                    active: false,
                    disabled: false
                });
                this.pagination.splice(8, count - (tempActive + 2), {
                    title: '...',
                    offset: 'showAfter',
                    active: false,
                    disabled: false
                });
            } else if (tempActive >= count - 2) {
                this.pagination.splice(2, tempActive - 3, {
                    title: '...',
                    offset: 'showBefore',
                    active: false,
                    disabled: false
                });
            }
        }
        this.pagination.push({
            title: 'След.',
            offset: (this.page.offset + this.page.limit),
            active: false,
            disabled: ((this.page.offset + this.page.limit) >= this.page.total) ? true : false
        });
        this.pagination.push({
            title: 'Послед.',
            offset: ((count - 1) * this.page.limit),
            active: false,
            disabled: ((this.page.offset + this.page.limit) >= this.page.total) ? true : false
        });
    }

    setPage(offset) {
        let activePage = 0;
        for (const item of this.pagination) {
            if (item.active) {
                activePage = Number(item.title);
            }
        }
        switch (offset) {
            case 'showBefore':
                this.page.offset = ((activePage - 4) * this.page.limit);
                break;
            case 'showAfter':
                this.page.offset = ((activePage + 2) * this.page.limit);
                break;
            default:
                this.page.offset = offset;
        }
        this.action.emit({method: 'setPage', value: this.page.offset});
    }

    setLimit() {
        this.action.emit({method: 'setLimit', value: this.currentLimit});
	}
}
