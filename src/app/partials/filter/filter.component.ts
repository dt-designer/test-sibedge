import {Component, EventEmitter, Input, Output} from '@angular/core';

@Component({
    selector: 'app-filter',
	templateUrl: './filter.component.html',
	styleUrls: ['./filter.component.css']
})
export class FilterComponent {

	@Input() filterFields: any;
    @Output() action: EventEmitter<any> = new EventEmitter();

    filterItemChanged() {
		this.action.emit({method: 'setFilterField'})
	}

	filterCleared() {
    	for (let item of this.filterFields) {
    		item.value = '';
		}
        this.action.emit({method: 'filterCleared'})
	}
}
