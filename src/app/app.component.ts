import {Component, OnInit} from '@angular/core';
import {ApiService} from "./services/api.service";

import {AppConfigs} from "./app.configs";
import {outOfBoundsError} from "@angular/core/src/di/reflective_errors";

@Component({
    selector: 'my-app',
	templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

    appLoaded: boolean = false;
    currentTime: any = new Date();

    filterFields = AppConfigs.filterFields; // Настрока полей фильтра
    tableCols = AppConfigs.tableCols; // Настройка полей таблицы

    pageLimits: any = AppConfigs.pageLimits; // Варианты лимитов для страницы
    currentLimit: number = 10;
    pageTotalItems: number = 0;
    pagination: any = {
        limit: this.currentLimit,
        offset: 0,
        total: 0
    };

    usersArray: any = []; // Массив хранящий все строки
    usersResult: any = []; // Выводимый массив
    usersFiltered: any = null; // Если пользователи отфильтрованы

    constructor(
        private api: ApiService
    ) {}

    ngOnInit() {
        this.api.getUsers().subscribe((data) => {
            this.pageTotalItems = data.info.results;
            this.pagination.total = this.pageTotalItems;
            for (let item of data.results) {
                item['dateTime'] = new Date(item.dob).getTime();
            }
            this.usersArray = data.results;
            this.getCurrentResult(this.usersArray);
        });
    }

    getCurrentResult(usersArray) {
        this.usersResult = usersArray.slice(this.pagination.offset, this.pagination.limit + this.pagination.offset);
        this.appLoaded = true;
    }

    setPage(event) {
        switch (event.method) {
            case "setPage":
                this.pagination.offset = Number(event.value);
                break;
            case "setLimit":
                this.pagination.limit = Number(event.value);
                this.currentLimit = Number(event.value);
                break;
        }
        this.currentTime = new Date();
        this.getCurrentResult(this.usersFiltered ? this.usersFiltered : this.usersArray);
    }

    filterAction(event) {
        this.pagination.offset = 0;
        this.currentTime = new Date();
        switch (event.method) {
            case "filterCleared":
                this.pagination.total = this.pageTotalItems;
                this.usersFiltered = null;
                this.getCurrentResult(this.usersArray);
                break;
            case "setFilterField":
                let userFiltered = this.usersFiltered ? this.usersFiltered : this.usersArray;
                let minFrom = 0;
                let emptyCount = 0;
                for (let item of this.filterFields) {
                    if(item.value) {
                        switch (item.id) {
                            case "last_name":
                                this.usersFiltered = userFiltered.filter(p => p.name.last.toLocaleLowerCase().includes(item.value.toLocaleLowerCase()));
                                break;
                            case "phone":
                                this.usersFiltered = userFiltered.filter(p => p.phone.includes(item.value));
                                break;
                            case "location_city":
                                this.usersFiltered = userFiltered.filter(p => p.location.city.toLocaleLowerCase().includes(item.value.toLocaleLowerCase()));
                                break;
                            case "date_from":
                                minFrom = new Date(item.value).getTime();
                                this.usersFiltered = userFiltered.filter(p => (minFrom < p.dateTime));
                                break;
                            case "date_to":
                                if(new Date(item.value).getTime() > minFrom) {
                                    this.usersFiltered = userFiltered.filter(p => (new Date(item.value).getTime() > p.dateTime));
                                }
                                break;
                        }
                    } else {
                        emptyCount++
                    }
                }
                console.log(this.usersFiltered);
                console.log('=================');
                if (emptyCount === this.filterFields.length) {
                    this.usersFiltered = null;
                }
                this.pagination.total = this.usersFiltered ? this.usersFiltered.length : this.usersArray.length;
                this.getCurrentResult(this.usersFiltered ? this.usersFiltered : this.usersArray);
                break;
        }
    }

}
