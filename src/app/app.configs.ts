export const AppConfigs = {
    filterFields: [
        {
            id: 'last_name',
            type: 'text',
            label: 'Фамилия',
            prefix: '',
            value: '',
            placeholder: 'Фильтр по Фамилии'
        },
        {
            id: 'phone',
            type: 'number',
            label: 'Телефон',
            prefix: '',
            value: '',
            placeholder: 'Фильтр по Номеру телефона'
        },
        {
            id: 'location_city',
            type: 'text',
            label: 'Город',
            prefix: '',
            value: '',
            placeholder: 'Фильтр по Городу'
        },
        {
            id: 'date_from',
            type: 'date',
            label: 'Дата рождения',
            prefix: 'От',
            value: '',
            placeholder: 'Фильтр по дате рождения'
        },
        {
            id: 'date_to',
            type: 'date',
            label: '',
            prefix: 'До',
            value: '',
            placeholder: ''
        }
    ],
    tableCols: [
        {
            id: 'name',
            title: 'Имя',
            type: 'name' // text, name, email, location, gender, date, picture
        },
        {
            id: 'login',
            title: 'Логин',
            type: 'username'
        },
        {
            id: 'gender',
            title: 'Пол',
            type: 'gender'
        },
        {
            id: 'dob',
            title: 'Д/Р',
            type: 'date',
            params: {
                date: true,
                time: false
            }
        },
        {
            id: 'email',
            title: 'E-Mail',
            type: 'email'
        },
        {
            id: 'location',
            title: 'Локация',
            type: 'location'
        },
        {
            id: 'phone',
            title: 'Телефон',
            type: 'text'
        },
        {
            id: 'picture',
            title: 'Фото',
            type: 'picture'
        },
        {
            id: 'registered',
            title: 'Регистрация',
            type: 'date',
            params: {
                date: true,
                time: true
            }
        }
    ],
    pageLimits: [
        10, 20, 30
    ]
};