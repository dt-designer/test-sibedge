import {NgModule}      from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {HttpClientModule} from "@angular/common/http";
import {FormsModule}   from '@angular/forms';

import {AppComponent}  from './app.component';
import {HeaderComponent} from "./partials/header/header.component";
import {FilterComponent} from "./partials/filter/filter.component";
import {PaginationComponent} from "./partials/pegination/pagination.component";

import {ApiService} from "./services/api.service";

import {DateDirective} from "./directives/date.directive";
import {LocationDirective} from "./directives/location.directive";

@NgModule({
    imports: [
        BrowserModule,
        HttpClientModule,
        FormsModule
    ],
    declarations: [
        AppComponent,
        HeaderComponent,
        FilterComponent,
        PaginationComponent,
        DateDirective,
        LocationDirective
    ],
    providers: [
        ApiService
    ],
    bootstrap: [
        AppComponent
    ]
})
export class AppModule { }
