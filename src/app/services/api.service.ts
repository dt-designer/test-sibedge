import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";

@Injectable()
export class ApiService {

    constructor(
        private http: HttpClient
    ) {}

    public getUsers():any {
        return this.http.get('./src/assets/users.json');
    };

}
